@extends('layouts.template')

@section('body')
    <div class="container">
        <div class="main-container">
            @if (count($errors) > 0)
                <ul style="color: red;">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            @if(session('type'))
            <div class="row">
                <div class="alert alert-{{session('type')}} alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{session('message')}}
                </div>
            </div>
            @endif
            <div class="row">
                <h1>Update Directory</h1>
            </div>
            <form action="{{url('admin/directories',$directory->id)}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-sm-6">
                        <label for="" class="control-label">Logo</label>
                        <div class="form-group">
                            <label for="logoFile" class="">
                                <img id="logo_file_preview" src="{{asset('/storage/'.$directory->logo)}}" alt="logo image" />
                                <input type='file' name="logo" id="logoFile" />
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="" class="control-label">Banner</label>
                            <label for="headerFile" class="">
                                <img id="header_file_preview" src="{{asset('/storage/'.$directory->header)}}" alt="cover image" />
                                <input type='file' name="banner" id="headerFile" />
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" id="name" name="name" class="form-control" placeholder="Name" required value="{{$directory->name}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="category" class="control-label">Category</label>
                            <select id="category" name="category" class="form-control" required>
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if($directory->category_id == $category->id) selected @endif>{{str_replace('-',' ',ucwords($category->name))}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="fb_link" class="control-label">Facebook Link</label>
                            <input type="url" id="fb_link" name="fb_link" class="form-control" placeholder="Facebook" value="{{$directory->fb_link}}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="insta_link" class="control-label">Instagram Link</label>
                            <input type="url" id="insta_link" name="insta_link" class="form-control" placeholder="Instagram" value="{{$directory->instagram_link}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="" class="control-label">Description</label>
                            <textarea class="form-control" name="description" placeholder="Description" required>{{$directory->description}}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="" class="control-label">Gallery Images</label>
                            <input type="file" id="images" name="images[]" onchange="preview_images();" multiple />
                            <div class="row" id="image_preview">
                                @isset($directory->images)
                                    @foreach($directory->images as $image)
                                            <div class='col-md-3'><img class='img-responsive' src='{{asset('/storage/'.$image->link)}}'></div>
                                    @endforeach
                                @endisset
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="status" class="control-label">Status</label>
                            <select class="form-control" id="status" name="status" required>
                                <option value="">Select any option</option>
                                <option value="active" @if($directory->status == 'active') selected @endif>Active</option>
                                <option value="active" @if($directory->status == 'pending') selected @endif>Pending</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="favourite" class="control-label">Favourtie</label>
                            <select class="form-control" id="favourite" name="favourite" required>
                                <option value="">Select any option</option>
                                <option value="on" @if($directory->favourite == 'on') selected @endif>Yes</option>
                                <option value="off" @if($directory->favourite == 'off') selected @endif>No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group pull-right">
                            <input type="submit" class="btn-sm blue-btn et_pb_button " value="Update">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        function readURL(input,img) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    jQuery(img).attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        jQuery("#logoFile").change(function () {
            readURL(this,jQuery('#logo_file_preview'));
        });
        jQuery("#headerFile").change(function () {
            readURL(this,jQuery('#header_file_preview'));
        });
        function preview_images()
        {
            var total_file=document.getElementById("images").files.length;
            for(var i=0;i<total_file;i++)
            {
                jQuery('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
            }
        }
    </script>
@endsection
