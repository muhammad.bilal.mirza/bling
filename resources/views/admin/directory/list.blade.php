@extends('layouts.template')

@section('body')
    <div class="container">
        <div class="main-container">
            @if(session('type'))
                <div class="row">
                    <div class="alert alert-{{session('type')}} alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{session('message')}}
                    </div>
                </div>
            @endif
            <div class="row">
                <h1>Directories</h1>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>User</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Favourite</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($directories as $directory)
                        <tr>
                            <td>{{$directory->user->name}}</td>
                            <td>{{$directory->name}}</td>
                            <td>{{ucwords($directory->category->name)}}</td>
                            <td><span class="btn btn-xs @if($directory->status == 'active') btn-success @elseif($directory->status == 'pending') btn-warning @endif">{{ucfirst($directory->status)}}</span></td>
                            <td><span class="btn btn-xs @if($directory->favourite == 'on') btn-success @elseif($directory->favourite == 'off') btn-default @endif">@if($directory->favourite == 'on') Yes @elseif($directory->favourite == 'off') No @endif</span></td>
                            <td>
                                <a href="{{url('admin/edit-directory',$directory->id)}}" class="btn btn-xs btn-primary">Edit</a>
                                <a href="{{url('admin/delete-directories',$directory->id)}}" class="btn btn-xs btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection