@extends('layouts.template')

@section('body')
    <div id="main-content">
        <div class="container">
            <div id="content-area" class="clearfix">
                <div id="left-area">

                    <article id="post-27077" class="et_pb_post post-27077 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">

                        <a class="entry-featured-image-url" href="https://www.modernstationeryco.com/7-things-you-can-do-to-see-results-quickly/">
                            <img src="./images/blog/quick-results.png" alt="7 Things You Can Do to See Results Quickly" width="1080" height="675"> </a>

                        <h2 class="entry-title"><a href="https://www.modernstationeryco.com/7-things-you-can-do-to-see-results-quickly/">7 Things You Can Do to See Results Quickly</a></h2>

                        <p class="post-meta"> by <span class="author vcard"><a href="https://www.modernstationeryco.com/author/modernsco/" title="Posts by ModernSCo" rel="author">ModernSCo</a></span> | <span class="published">May 17, 2018</span> | <a href="https://www.modernstationeryco.com/category/uncategorized/" rel="category tag">Uncategorized</a></p>Goal Setting: 7 Things You Can Do to See Results Quickly Tired of waiting to see results? If you’re anything like me – you want results and you want them now. In this post you’ll learn 7 Things you can do to help you quickly achieve your goals, kick-start the process...
                    </article>
                    <!-- .et_pb_post -->

                    <article id="post-26599" class="et_pb_post post-26599 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">

                        <a class="entry-featured-image-url" href="https://www.modernstationeryco.com/10-of-the-best-2017-christmas-cards/">
                            <img src="./images/blog/christmas-cards.jpg" alt="10 2017 Christmas cards you’ll actually want to send this year" width="1080" height="675"> </a>

                        <h2 class="entry-title"><a href="https://www.modernstationeryco.com/10-of-the-best-2017-christmas-cards/">10 2017 Christmas cards you’ll actually want to send this year</a></h2>

                        <p class="post-meta"> by <span class="author vcard"><a href="https://www.modernstationeryco.com/author/modernsco/" title="Posts by ModernSCo" rel="author">ModernSCo</a></span> | <span class="published">Nov 12, 2017</span> | <a href="https://www.modernstationeryco.com/category/uncategorized/" rel="category tag">Uncategorized</a></p>There are only six short weeks until Christmas! That means if you’re planing on sending Christmas cards this year, you better do it soon! Luckily we have scoured our favourite stationery designers to bring you 10 of the best 2017 Christmas cards you’ll...
                    </article>
                    <!-- .et_pb_post -->

                </div>
                <!-- #left-area -->

            </div>
            <!-- #content-area -->
        </div>
        <!-- .container -->
    </div>
    <!-- #main-content -->
@endsection
