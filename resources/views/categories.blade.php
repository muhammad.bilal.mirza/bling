@extends('layouts.template')

@section('body')
    <div id="main-content">

        <article id="post-688" class="post-688 page type-page status-publish hentry">

            <div class="entry-content">
                <div class="mailmunch-forms-before-post" style="display: none !important;"></div>
                <div class="et_pb_section et_pb_section_0 et_section_regular">

                    <div class="et_pb_row et_pb_row_0 et_pb_row_4col">
                        @foreach($categories as $category)
                            <div class="et_pb_column et_pb_column_1_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">
                                <div class="et_pb_module et_pb_image et_pb_image_0 et_always_center_on_mobile text-center">
                                    <a href="{{url('directories',$category->id)}}">
                                        <div class="bg-img" style="background-image: url({{asset('/images/categs/'.$category->name.'.jpg')}});">
                                        </div>
                                        <h2 class="mt-5">{{str_replace('-',' ',ucwords($category->name))}}</h2>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>

                </div>
                <!-- .et_pb_section -->
            </div>
            <!-- .entry-content -->

        </article>
        <!-- .et_pb_post -->

    </div>
    <!-- #main-content -->
@endsection