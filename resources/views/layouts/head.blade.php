<head>
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Browse our boutique online stationery directory, we have a large, handpicked collection of Australia&#39;s best boutique stationery businesses.">
    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Boutique online stationery directory - The Bling Collective Co">
    <meta property="og:description" content="Browse our boutique online stationery directory, we have a large, handpicked collection of Australia&#39;s best boutique stationery businesses.">
    <meta property="og:site_name" content="The Bling Collective Co">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- End Meta Tags -->
    <title>Boutique online stationery directory - The Bling Collective Co</title>
    <!-- Stylings -->
    <link rel="stylesheet" id="app-style-css" href="{{asset('css/app.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="parent-style-css" href="{{asset('css/parent-style.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="main-style-css" href="{{asset('css/main-style.css')}}" type="text/css" media="all">
    <link rel="stylesheet" id="custom-style-css" href="{{asset('css/custom.css')}}" type="text/css" media="all">
    <!-- End Stylings -->

    <!-- JavaScripts -->
    <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/cookielawinfo.js')}}"></script>
    <!-- END JavaScripts -->
</head>