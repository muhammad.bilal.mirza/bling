<div id="top-header">
    <div class="container clearfix">
        <div id="et-secondary-menu">
            <ul class="et-social-icons">
                <li class="et-social-icon et-social-facebook">
                    <a href="https://www.facebook.com/Modern-Stationery-Co-1408429312502230%20" class="icon">
                        <span>Facebook</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-instagram">
                    <a href="https://www.instagram.com/modernstationeryco/" class="icon">
                        <span>LinkedIn</span>
                    </a>
                </li>

                <li class="et-social-icon et-social-pinterest">
                    <a href="https://au.pinterest.com/ModStationeryCo/" class="icon">
                        <span>LinkedIn</span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- #et-secondary-menu -->

    </div>
    <!-- .container -->
</div>
<!-- #top-header -->

<header id="main-header" data-height-onload="253" data-height-loaded="true" data-fixed-height-onload="123">
    <div class="container clearfix et_menu_container">
        <div class="logo_container">
            <span class="logo_helper"></span>
            <a href="{{url('/')}}">
                <img src="{{asset('/images/logo.png')}}" alt="Modern Stationery Co" id="logo" data-height-percentage="90" data-actual-width="421" data-actual-height="83">
            </a>
        </div>
        <div id="et-top-navigation" data-height="200" data-fixed-height="40">
            <nav id="top-menu-nav">
                <ul id="top-menu" class="nav">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="{{url('/')}}">Home</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home"><a href="{{url('/blog')}}">Blog</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('/about')}}">About</a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('/contact')}}">Contact Us</a></li>
                    <li  class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-has-children"><a>View Directory</a>
                        <ul class="sub-menu">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('categories')}}"><small style="font-size: 85%">Shop by Category</small></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('directories')}}"><small style="font-size: 85%">Shop by Boutique</small></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('list-with-us')}}"><small style="font-size: 85%">List with Us</small></a></li>
                        </ul>
                    </li>
                    <li id="menu-item-337" class="menu-item menu-item-type-post_type menu-item-object-page  menu-item-has-children"><a>Account</a>
                        <ul class="sub-menu">
                            @guest()
                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('login')}}"><small style="font-size: 85%">Login</small></a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('register')}}"><small style="font-size: 85%">Register</small></a></li>
                            @endguest
                            @auth()
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{url('user-directories')}}"><small style="font-size: 85%">Directories</small></a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page">
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <small style="font-size: 85%">Logout</small>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                            @endauth
                        </ul>
                    </li>
                </ul>
            </nav>

            <div id="et_top_search">
                <span id="et_search_icon"></span>
            </div>

            <div id="et_mobile_nav_menu">
                <div class="mobile_nav closed">
                    <span class="select_page">Select Page</span>
                    <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                    <ul id="mobile_menu" class="et_mobile_menu">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-45 et_first_mobile_item"><a href="{{url('/')}}">Home</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-44"><a href="{{url('/about')}}">About</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-494"><a href="{{url('/blog')}}">Blog</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-43"><a href="{{url('/contact')}}">Contact Us</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-337"><a href="https://www.modernstationeryco.com/view-directory/">View Directory</a>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-688 current_page_item menu-item-847"><a href="{{url('categories')}}">Shop by Category</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-335"><a href="{{url('directories')}}">Shop by Boutique</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26983"><a href="{{url('list-with-us')}}">List with Us</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #et-top-navigation -->
    </div>
    <!-- .container -->
    <div class="et_search_outer">
        <div class="container et_search_form_container">
            <form role="search" method="get" class="et-search-form" action="https://www.modernstationeryco.com/">
                <input type="search" class="et-search-field" placeholder="Search …" value="" name="s" title="Search for:"> </form>
            <span class="et_close_search_field"></span>
        </div>
    </div>
</header>
<!-- #main-header -->