<!doctype html>
<html lang="en">
    @include('layouts/head')
    <body class="page-template-default page page-id-688 ctct-Divi et_pb_button_helper_class et_fullwidth_secondary_nav et_non_fixed_nav et_show_nav et_cover_background et_secondary_nav_enabled et_pb_gutter windows et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_centered et_pb_pagebuilder_layout et_smooth_scroll et_right_sidebar et_divi_theme et_minified_js et_minified_css chrome">
        <div id="page-container">
            @include('layouts/header')
            @yield('body')
        </div>
        @include('layouts/footer')
    </body>
</html>