<span class="et_pb_scroll_top et-pb-icon et-hidden" style="display: inline;"></span>
<footer id="main-footer">
    <div class="container">
        <div id="footer-widgets" class="clearfix">
            <div class="footer-widget">
                <div id="nav_menu-5" class="fwidget et_pb_widget widget_nav_menu">
                    <h4 class="title">Policies</h4>
                    <div class="menu-policies-container">
                        <ul id="menu-policies" class="menu">
                            <li id="menu-item-353" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-353"><a href="https://www.modernstationeryco.com/privacy/">Privacy</a></li>
                            <li id="menu-item-354" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-354"><a href="https://www.modernstationeryco.com/terms-and-conditions/">Terms and conditions</a></li>
                            <li id="menu-item-491" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-491"><a href="https://www.modernstationeryco.com/site-map/">Site map</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end .fwidget -->
            </div>
            <!-- end .footer-widget -->
            <div class="footer-widget">
                <div id="nav_menu-6" class="fwidget et_pb_widget widget_nav_menu">
                    <h4 class="title">Collection</h4>
                    <div class="menu-modern-stationery-co-container">
                        <ul id="menu-modern-stationery-co" class="menu">
                            <li id="menu-item-356" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-356"><a href="./categories.php">Shop By Category</a></li>
                            <li id="menu-item-355" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-355"><a href="./boutique.php">Shop by boutique</a></li>
                            <li id="menu-item-26981" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26981"><a href="./list-with-us.php">List with Us</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end .fwidget -->
            </div>
            <!-- end .footer-widget -->
            <div class="footer-widget"></div>
            <!-- end .footer-widget -->
            <div class="footer-widget last">
                <div id="mc4wp_form_widget-3" class="fwidget et_pb_widget widget_mc4wp_form_widget">
                    <h4 class="title">Join our mailing list</h4>

                    <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-360 mc4wp-form-theme mc4wp-form-theme-light" method="post" data-id="360" data-name="Subscribe">
                        <div class="mc4wp-form-fields">
                            <p>
                                <label>First Name</label>
                                <input type="text" name="FNAME" required="">
                            </p>
                            <p>
                                <label>Email address: </label>
                                <input type="email" name="EMAIL" placeholder="" required="">
                            </p>

                            <p>
                                <input type="submit" value="Sign up">
                            </p>
                        </div>
                        <label style="display: none !important;">Leave this field empty if you're human:
                            <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off">
                        </label>
                        <input type="hidden" name="_mc4wp_timestamp" value="1533738562">
                        <input type="hidden" name="_mc4wp_form_id" value="360">
                        <input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1">
                        <div class="mc4wp-response"></div>
                    </form>
                    <!-- / MailChimp for WordPress Plugin -->
                </div>
                <!-- end .fwidget -->
            </div>
            <!-- end .footer-widget -->
        </div>
        <!-- #footer-widgets -->
    </div>
    <!-- .container -->

    <div id="footer-bottom">
        <div class="container clearfix">
            <ul class="et-social-icons">

                <li class="et-social-icon et-social-facebook">
                    <a href="https://www.facebook.com/Modern-Stationery-Co-1408429312502230%20" class="icon">
                        <span>Facebook</span>
                    </a>
                </li>
                <li class="et-social-icon et-social-instagram">
                    <a href="https://www.instagram.com/modernstationeryco/" class="icon">
                        <span>LinkedIn</span>
                    </a>
                </li>

                <li class="et-social-icon et-social-pinterest">
                    <a href="https://au.pinterest.com/ModStationeryCo/" class="icon">
                        <span>LinkedIn</span>
                    </a>
                </li>

            </ul>
            <div id="footer-info">Copyright Modern Stationery Co. | Designed By Business Jump</div>
        </div>
        <!-- .container -->
    </div>
</footer>
<!-- #main-footer -->