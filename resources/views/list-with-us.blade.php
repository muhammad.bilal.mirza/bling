@extends('layouts.template')

@section('body')
    <div id="main-content">

        <article id="post-26977" class="post-26977 page type-page status-publish hentry">

            <div class="entry-content">
                <div class="mailmunch-forms-before-post" style="display: none !important;"></div>
                <div class="et_pb_section et_pb_section_0 et_section_regular">

                    <div class="et_pb_row et_pb_row_0">
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">

                            <div class="et_pb_module et_pb_image et_pb_image_0 et_animated et-waypoint slideLeft et-animated" style="animation-duration: 500ms; animation-delay: 0ms; opacity: 0; animation-timing-function: ease-in-out; transform: translate3d(-20%, 0px, 0px);">

                                <span class="et_pb_image_wrap"><img src="./images/logo.png" alt=""></span>
                            </div>
                        </div>
                        <!-- .et_pb_column -->
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et-last-child">

                            <div class="et_pb_module et_pb_blurb et_pb_blurb_0 et_pb_bg_layout_light  et_pb_text_align_left  et_pb_blurb_position_top">

                                <div class="et_pb_blurb_content">

                                    <div class="et_pb_blurb_container">

                                        <div class="et_pb_blurb_description">
                                            <p>Hello and thank you for considering a listing with us. The aim of our directory is to curate a collection of the very best stationery and paper related goods Australia has to offer. We are so excited to have you on board. By listing on our exclusive directory you have the opportunity to get your business in front of an entirely new and growing audience.</p>
                                            <p>Please get in contact for more details and pricing.</p>
                                            <div class="mailmunch-forms-in-post-middle" style="display: none !important;"></div>
                                            <p>Welcome to the Modern Stationery Co. team and look forward to a successful relationship with you.</p>
                                        </div>
                                        <!-- .et_pb_blurb_description -->
                                    </div>
                                </div>
                                <!-- .et_pb_blurb_content -->
                            </div>
                            <!-- .et_pb_blurb -->
                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                                <a class="blue-btn et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="{{url('/contact')}}">Contact us</a>
                            </div>
                        </div>
                        <!-- .et_pb_column -->

                    </div>
                    <!-- .et_pb_row -->

                </div>
                <!-- .et_pb_section -->
                <div class="mailmunch-forms-after-post" style="display: none !important;"></div>
            </div>
            <!-- .entry-content -->

        </article>
        <!-- .et_pb_post -->

    </div>
    <!-- #main-content -->
@endsection
