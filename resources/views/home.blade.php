@extends('layouts.template')

@section('body')
    <div id="main-content">
        <article class="page type-page status-publish has-post-thumbnail hentry">
            <div class="entry-content">
                <div class="et_pb_section et_pb_section_0 et_section_regular">
                    <div class="et_pb_row et_pb_row_0">
                        <div class="et_pb_column et_pb_column_4_4 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough et-last-child">
                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_center">
                                <div class="et_pb_text_inner">
                                    <h1 class="text-center">Welcome to Modern Stationery Co.</h1>
                                    <h2>A Boutique online stationery directory</h2>
                                    <p>&nbsp;</p>
                                    <p><span style="background-color: #ffffff; text-align: left;">We are a boutique online stationery directory, featuring the finest stationery from around the web. Search for the perfect birthday card, a new notebook, planners and journals, wrapping paper, invitations, notebooks or prints.&nbsp;</span><span style="background-color: #ffffff; text-align: left;">Our unique set up allows you to shop for exactly what you are looking for in one place.&nbsp;</span>We are always on the lookout to expand our directory, please get in contact if you are looking to get your product in front of a whole new audience of potential customers.</p>
                                </div>
                            </div>
                            <!-- .et_pb_text -->
                        </div>
                        <!-- .et_pb_column -->

                    </div>
                    <!-- .et_pb_row -->

                </div>
                <!-- .et_pb_section -->
                <div class="et_pb_section et_pb_section_1 et_pb_equal_columns et_pb_specialty_fullwidth et_section_specialty">
                    <div class="et_pb_row et_pb_gutters1">
                        <div class="et_pb_column et_pb_column_1_3 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et_pb_column_single">
                            <div class="et_pb_with_border et_pb_module et_pb_cta_0 et_pb_promo et_pb_bg_layout_light  et_pb_text_align_center et_pb_no_bg">
                                <div class="et_pb_promo_description">
                                </div>
                                <div class="et_pb_button_wrapper"><a class="et_pb_button et_pb_promo_button" href="{{url('directories')}}">SHOP BY BOUTIQUE</a></div>
                            </div>
                        </div>
                        <!-- .et_pb_column -->
                        <div class="et_pb_column et_pb_column_2_3 et_pb_column_2   et_pb_specialty_column  et_pb_css_mix_blend_mode_passthrough et-last-child">

                            <div class="et_pb_row_inner et_pb_row_inner_0 et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_0   et-last-child">

                                    <div class="et_pb_with_border et_pb_module et_pb_cta_1 et_pb_promo et_pb_bg_layout_light  et_pb_text_align_center et_pb_no_bg">

                                        <div class="et_pb_promo_description">

                                        </div>
                                        <div class="et_pb_button_wrapper"><a class="et_pb_button et_pb_promo_button" href="{{url('/blog')}}">READ OUR BLOG</a></div>
                                    </div>
                                </div>
                                <!-- .et_pb_column -->

                            </div>
                            <!-- .et_pb_row_inner -->
                            <div class="et_pb_row_inner et_pb_row_inner_1 et_pb_gutters1">
                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_1  ">

                                    <div class="et_pb_with_border et_pb_module et_pb_cta_2 et_pb_promo et_pb_bg_layout_light  et_pb_text_align_center et_pb_no_bg">

                                        <div class="et_pb_promo_description">

                                        </div>
                                        <div class="et_pb_button_wrapper"><a class="et_pb_button et_pb_promo_button" href="{{url('/list-with-us')}}">JOIN OUR DIRECTORY</a></div>
                                    </div>
                                </div>
                                <!-- .et_pb_column -->
                                <div class="et_pb_column et_pb_column_1_3 et_pb_column_inner et_pb_column_inner_2   et-last-child">

                                    <div class="et_pb_with_border et_pb_module et_pb_cta_3 et_pb_promo et_pb_bg_layout_light  et_pb_text_align_center et_pb_no_bg">

                                        <div class="et_pb_promo_description">

                                        </div>
                                        <div class="et_pb_button_wrapper"><a class="et_pb_button et_pb_promo_button" href="{{url('/categories')}}">SHOP BY CATEGORY</a></div>
                                    </div>
                                </div>
                                <!-- .et_pb_column -->

                            </div>
                            <!-- .et_pb_row_inner -->
                        </div>
                        <!-- .et_pb_column -->
                    </div>
                    <!-- .et_pb_row -->

                </div>
                <!-- .et_pb_section -->

                <div class="entry-content">
                    <h1 style="text-align: center;">Our Favourite Designs</h1>
                    <div class="et_pb_section et_pb_section_0 et_section_regular">
                        <div class="et_pb_row et_pb_row_10 et_pb_row_5col">
                            @foreach($directories as $directory)
                                <div class="et_pb_column et_pb_column_1_4 et_pb_column_28    et_pb_css_mix_blend_mode_passthrough">

                                    <div class="et_pb_module et_pb_image et_pb_image_28 et_always_center_on_mobile">

                                        <a href="{{url('directory',$directory->id)}}">
                                                <span class="et_pb_image_wrap">
                                                    <img src="{{asset('/storage/'.$directory->logo)}}" alt="{{$directory->name}}">
                                                </span>
                                            <p class="description">
                                                {{ucfirst($directory->name)}}
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                            <!-- .et_pb_column -->
                        </div>
                    </div>
                    <!-- .et_pb_section -->
                </div>

                <!-- .et_pb_post -->
            </div>
            <!-- .entry-content -->

        </article>
        <!-- .et_pb_post -->

    </div>
@endsection
