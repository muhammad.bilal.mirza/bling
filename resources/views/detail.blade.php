@extends('layouts.template')

@section('body')
    <div id="main-content">
        <div class="entry-content">
            <div class="et_pb_section et_pb_section_0 et_pb_with_background et_pb_fullwidth_section et_section_regular" style="background-image: url('{{asset('/storage/'.$directory->header)}}')">

                <section class="et_pb_module et_pb_fullwidth_header et_pb_fullwidth_header_0 et_pb_bg_layout_dark et_pb_text_align_center">

                    <div class="et_pb_fullwidth_header_container center">
                        <div class="header-content-container center">
                            <div class="header-content">

                                <div class="et_pb_header_content_wrapper"> </div>
                                <a class="blue-btn et_pb_button et_pb_more_button et_pb_button_one" href="http://www.printark.com.au/">{{ucfirst($directory->name)}}</a>
                            </div>
                        </div>

                    </div>
                    <div class="et_pb_fullwidth_header_overlay"></div>
                    <div class="et_pb_fullwidth_header_scroll"></div>
                </section>

            </div>
            <!-- .et_pb_section -->
            <div class="et_pb_section et_pb_section_1 et_section_regular">

                <div class="et_pb_row et_pb_row_0">
                    <div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">

                        <div class="et_pb_module et_pb_image et_pb_image_0 et_always_center_on_mobile">

                            <span class="et_pb_image_wrap"><img src="{{asset('/storage/'.$directory->logo)}}" alt=""></span>
                        </div>
                    </div>
                    <!-- .et_pb_column -->
                    <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et-last-child">

                        <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">

                            <div class="et_pb_text_inner">
                                <h3>{{ucfirst($directory->name)}}</h3>
                                <div class="w2dc-field-content w2dc-field-description">
                                    <p>{{$directory->description}}</p>
                                </div>
                            </div>
                        </div>
                        <!-- .et_pb_text -->
                        <ul class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_0 clearfix et_pb_bg_layout_dark ">

                            <li class="et_pb_social_media_follow_network_0 et_pb_social_icon et_pb_social_network_link  et-social-facebook et_pb_social_media_follow_network_0">
                                <a href="{{$directory->fb_link}}" class="icon et_pb_with_border" title="Facebook" target="_blank"><span class="et_pb_social_media_follow_network_name">Facebook</span></a>
                            </li>
                            <li class="et_pb_social_media_follow_network_1 et_pb_social_icon et_pb_social_network_link  et-social-instagram et_pb_social_media_follow_network_1">
                                <a href="{{$directory->insta_link}}" class="icon et_pb_with_border" title="Instagram" target="_blank"><span class="et_pb_social_media_follow_network_name">Instagram</span></a>
                            </li>
                        </ul>
                        <!-- .et_pb_counters -->
                    </div>
                    <!-- .et_pb_column -->

                </div>
                <!-- .et_pb_row -->
                <div class="et_pb_row et_pb_row_1">
                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_2    et_pb_css_mix_blend_mode_passthrough et-last-child">

                        <div class="et_pb_module et_pb_gallery et_pb_gallery_0 et_pb_bg_layout_light  et_pb_gallery_grid">
                            <div class="et_pb_gallery_items et_post_gallery clearfix" data-per_page="4">
                            @foreach($directory->images as $image)
                                    <div class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light" style="display: block;">
                                        <div class="et_pb_gallery_image landscape">
                                            <a href="{{asset('/storage/'.$image->link)}}" title="{{$directory->name}}">
                                                <img src="{{asset('/storage/'.$image->link)}}" alt="{{$directory->name}}">
                                                <span class="et_overlay"></span>
                                            </a>
                                        </div>
                                    </div>
                            @endforeach
                            </div>
                            <!-- .et_pb_gallery_items -->
                        </div>
                        <!-- .et_pb_gallery -->
                        <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                            <a class="blue-btn et_pb_button et_pb_button_0 et_pb_bg_layout_dark" href="http://www.printark.com.au/" target="_blank">SHOP PRINT ARK</a>
                        </div>
                    </div>
                    <!-- .et_pb_column -->

                </div>
                <!-- .et_pb_row -->

            </div>
            <!-- .et_pb_section -->
        </div>
        <!-- .entry-content -->
    </div>
    <!-- #main-content -->
@endsection
