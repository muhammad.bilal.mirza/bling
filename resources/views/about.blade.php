@extends('layouts.template')

@section('body')
    <div id="main-content">

        <article id="post-39" class="post-39 page type-page status-publish hentry">

            <div class="entry-content">
                <div class="mailmunch-forms-before-post" style="display: none !important;"></div>
                <div class="et_pb_section et_pb_section_0 et_section_regular">

                    <div class="et_pb_row et_pb_row_0">
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">

                            <div class="et_pb_module et_pb_image et_pb_image_0 et_always_center_on_mobile">

                                <span class="et_pb_image_wrap"><img src="{{'../images/About-Me.jpg'}}" alt="Photo of Katie" title="Katies Headshot"></span>
                            </div>
                        </div>
                        <!-- .et_pb_column -->
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et-last-child">

                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">

                                <div class="et_pb_text_inner">
                                    <h2>About Modern Stationery Co.</h2>
                                    <p>Hello and welcome to Modern Stationery Co! Thank you so much for stopping by.&nbsp;I love stationery and am passionate about supporting the small, independent stationery designers. Be sure to check out our directory of amazing boutique stationery designers.</p>
                                    <p class="p1">My name is Katie and I am the owner of Modern Stationery Co. I have always been a bit of a stationery nut, I don’t quite remember when it started, but most of my childhood memories have some sort of craft or creative activity in them.</p>
                                    <p class="p1">Growing up in the early 90’s in Cairns there wasn’t much stationery variety. I think Big W was about as exciting as you could get. So I have fond memories of the end of the summer holidays, holed up with the other girls in my street (in someones air-conditioning), lovingly covering our school books in the latest teen heart throbs, making what we thought was our own beautiful stationery. As I got older and we moved south to the big smoke the stationery we could get started getting a little more exciting. I remember pastel gel pens coming out, and Officeworks, and then stores like Kikki K and Typo opened and my world changed!</p>
                                    <p class="p1">I went down a bit of a creative path at school, which naturally lead me to study Graphic Design at Uni. I think this is where I first started to take note of all the amazingly talented stationery designers we have access to.</p>
                                    <div class="mailmunch-forms-in-post-middle" style="display: none !important;"></div>
                                    <p class="p1">I currently live in Brisbane with my Husband, two year old daughter and our Labrador Harvey. It was on maternity leave when I was exploring my work options that I launched Modern Stationery Co. I am passionate about supporting the small independent designers that I have discovered and I hope to make their lives a little easier by introducing my humble, loyal following to them.</p>
                                    <p>I am always on the lookout to expand the growing directory, please get in contact if you are looking to get your product in front of a whole new audience of potential customers.</p>
                                    <p>Make sure you follow us on social media to keep up to date on all the latest stationery trends and our finds from around the web.</p>
                                    <p>I love hearing from fellow stationery lovers, so please drop me a line if you discover something amazing that you think I should see.</p>
                                </div>
                            </div>
                            <!-- .et_pb_text -->
                            <div class="et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_center et_pb_module ">
                                <a class="et_pb_button et_pb_button_0 et_pb_bg_layout_light" href="contact.php">CONTACT US</a>
                            </div>
                        </div>
                        <!-- .et_pb_column -->

                    </div>
                    <!-- .et_pb_row -->

                </div>
                <!-- .et_pb_section -->
                <div class="mailmunch-forms-after-post" style="display: none !important;"></div>
            </div>
            <!-- .entry-content -->

        </article>
        <!-- .et_pb_post -->

    </div>
    <!-- #main-content -->
@endsection
