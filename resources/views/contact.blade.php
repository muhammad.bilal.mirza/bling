@extends('layouts.template')

@section('body')
    <div id="main-content">

        <article id="post-41" class="post-41 page type-page status-publish hentry">

            <div class="entry-content">
                <div class="mailmunch-forms-before-post" style="display: none !important;"></div>
                <div class="et_pb_section et_pb_section_0 et_section_regular">

                    <div class="et_pb_row et_pb_row_0">
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_0    et_pb_css_mix_blend_mode_passthrough">

                            <div class="et_pb_module et_pb_text et_pb_text_0 et_pb_bg_layout_light  et_pb_text_align_left">

                                <div class="et_pb_text_inner">
                                    <div id="panel-21-0-0-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="1">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget">
                                                <div id="panel-46-0-0-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="1">
                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                        <div class="siteorigin-widget-tinymce textwidget"><strong>Online Stationery Directory</strong>
                                                            <br> Brisbane, Queensland
                                                            <br> Australia
                                                        </div>
                                                        <div class="siteorigin-widget-tinymce textwidget"></div>
                                                        <div class="siteorigin-widget-tinymce textwidget">katie@modernstationeryco.com</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .et_pb_text -->
                            <div class="et_pb_module et_pb_text et_pb_text_1 et_pb_bg_layout_light  et_pb_text_align_left">

                                <div class="et_pb_text_inner">
                                    <div id="panel-21-0-0-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="1">
                                        <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                            <div class="siteorigin-widget-tinymce textwidget">
                                                <div id="panel-46-0-0-1" class="so-panel widget widget_sow-editor panel-last-child" data-index="1">
                                                    <div class="so-widget-sow-editor so-widget-sow-editor-base">
                                                        <p><strong>Business&nbsp;Directory Enquiries</strong></p>
                                                        <div class="mailmunch-forms-in-post-middle" style="display: none !important;"></div>
                                                        <p>&nbsp;</p>
                                                        <div class="siteorigin-widget-tinymce textwidget">For business enquiries, please contact us on&nbsp;katie@modernstationeryco.com
                                                            <br> We will get back to you shortly.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- .et_pb_text -->
                        </div>
                        <!-- .et_pb_column -->
                        <div class="et_pb_column et_pb_column_1_2 et_pb_column_1    et_pb_css_mix_blend_mode_passthrough et-last-child">

                            <div id="et_pb_contact_form_0" class="et_pb_module et_pb_contact_form_0 et_pb_contact_form_container clearfix" data-form_unique_num="0">

                                <h1 class="et_pb_contact_main_title">Contact Us</h1>
                                <div class="et-pb-contact-message"></div>

                                <div class="et_pb_contact">
                                    <form class="et_pb_contact_form clearfix" method="post" action="https://www.modernstationeryco.com/contact-us/">
                                        <p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_half" data-id="name" data-type="input">

                                            <label for="et_pb_contact_name_1" class="et_pb_contact_form_label">Name</label>
                                            <input type="text" id="et_pb_contact_name_1" class="input" value="" name="et_pb_contact_name_1" data-required_mark="required" data-field_type="input" data-original_id="name" placeholder="Name">
                                        </p>
                                        <p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_half et_pb_contact_field_last" data-id="email" data-type="email">

                                            <label for="et_pb_contact_email_1" class="et_pb_contact_form_label">Email Address</label>
                                            <input type="text" id="et_pb_contact_email_1" class="input" value="" name="et_pb_contact_email_1" data-required_mark="required" data-field_type="email" data-original_id="email" placeholder="Email Address">
                                        </p>
                                        <p class="et_pb_contact_field et_pb_contact_field_2 et_pb_contact_field_last" data-id="message" data-type="text">

                                            <label for="et_pb_contact_message_1" class="et_pb_contact_form_label">Message</label>
                                            <textarea name="et_pb_contact_message_1" id="et_pb_contact_message_1" class="et_pb_contact_message input" data-required_mark="required" data-field_type="text" data-original_id="message" placeholder="Message"></textarea>
                                        </p>
                                        <input type="hidden" value="et_contact_proccess" name="et_pb_contactform_submit_0">
                                        <input type="text" value="" name="et_pb_contactform_validate_0" class="et_pb_contactform_validate_field">
                                        <div class="et_contact_bottom_container">
                                            <button type="submit" class="et_pb_contact_submit et_pb_button">Submit</button>
                                        </div>
                                        <input type="hidden" id="_wpnonce-et-pb-contact-form-submitted" name="_wpnonce-et-pb-contact-form-submitted" value="780577b6bb">
                                        <input type="hidden" name="_wp_http_referer" value="/contact-us/">
                                    </form>
                                </div>
                                <!-- .et_pb_contact -->
                            </div>
                            <!-- .et_pb_contact_form_container -->

                        </div>
                        <!-- .et_pb_column -->

                    </div>
                    <!-- .et_pb_row -->

                </div>
                <!-- .et_pb_section -->
                <div class="mailmunch-forms-after-post" style="display: none !important;"></div>
            </div>
            <!-- .entry-content -->

        </article>
        <!-- .et_pb_post -->

    </div>
    <!-- #main-content -->
@endsection
