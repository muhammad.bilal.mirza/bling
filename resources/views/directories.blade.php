@extends('layouts.template')

@section('body')
    <div id="main-content">
        <div class="entry-content">
            <div class="et_pb_section et_pb_section_0 et_section_regular">
                <div class="et_pb_row et_pb_row_10 et_pb_row_4col">
                    @foreach($directories as $directory)
                        <div class="et_pb_column et_pb_column_1_4 et_pb_column_28 et_pb_css_mix_blend_mode_passthrough">
                            <div class="et_pb_module et_pb_image et_pb_image_28 et_always_center_on_mobile">
                                <a href="{{url('directory',$directory->id)}}">
                                                <span class="et_pb_image_wrap">
                                                    <img src="{{asset('/storage/'.$directory->logo)}}" alt="">
                                                </span>
                                    <p class="description">{{$directory->name}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                <!-- .et_pb_column -->
                </div>

            </div>
            <!-- .et_pb_section -->
        </div>
        <!-- .entry-content -->
    </div>
@endsection