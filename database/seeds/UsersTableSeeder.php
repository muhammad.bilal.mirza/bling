<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::created(['name'=>'admin','email'=>'admin@admin.com','password'=>\Illuminate\Support\Facades\Hash::make('adminadmin'),'role'=>'admin']);
    }
}
