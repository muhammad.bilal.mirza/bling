<?php

namespace App\Http\Controllers;

use App\Category;
use App\Directory;
use App\DirectoryImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function directories(){
        $directories= Auth::user()->directories;
        return view('user.directory.list',compact('directories'));
    }
    public function addDirectory(){
        $categories= Category::all();
        return view('user.directory.add',compact('categories'));
    }
    public function storeDirectory(Request $request){
        $rules=[
            'name'=> 'required|string',
            'category'=> 'required|integer',
            'description'=> 'required|string',
            'fb_link'=> 'nullable|string',
            'insta_link'=> 'nullable|string',
            'logo'=> 'required|image|mimes:jpeg,jpg,png|max:1000',
            'banner'=> 'nullable|image|mimes:jpeg,jpg,png|max:1000',
        ];
        if(!empty($request->images)) {
            foreach ($request->images as $index => $item) {
                $rules['images.' . $index] = 'image|mimes:jpeg,bmp,png|max:1000';
            }
        }
        $this->validate($request,$rules);
        $data=[
            'user_id'=> Auth::user()->id,
            'name'=> $request->name,
            'category_id'=> $request->category,
            'description'=> $request->description,
            'fb_link'=> $request->fb_link,
            'instagram_link'=> $request->insta_link,
        ];
        $directory=Directory::create($data);
        if(!empty($request->logo)){
            if($path=$request->file('logo')->store('logos')){
                $directory->logo= $path;
            }
        }
        if(!empty($request->banner)){
            if($path=$request->file('banner')->store('banners')){
                $directory->header= $path;
            }
        }
        if(!empty($request->images)){
            foreach ($request->images as $image) {
                $path = $image->store('gallery');
                DirectoryImage::create([
                    'directory_id' => $directory->id,
                    'link' => $path
                ]);
            }
        }
        if ($directory->save()){
            return redirect('directories')->with(['type'=>'success', 'message'=>'Directory Added Successfully.']);
        }else{
            return redirect()->back()->with(['type'=>'error', 'message'=>'Directory Could not Added.']);
        }
    }
    public function editDirectory($directory){
        $directory=Directory::findOrFail($directory);
        $categories= Category::all();
        return view('user.directory.edit',compact('directory','categories'));
    }
    public function updateDirectory($directory,Request $request){
        $directory=Directory::findOrFail($directory);
        $rules=[
            'name'=> 'required|string',
            'category'=> 'required|integer',
            'description'=> 'required|string',
            'fb_link'=> 'nullable|string',
            'insta_link'=> 'nullable|string',
            'logo'=> 'image|mimes:jpeg,jpg,png|max:1000',
            'banner'=> 'image|mimes:jpeg,jpg,png|max:1000',
        ];
        if(!empty($request->images)) {
            foreach ($request->images as $index => $item) {
                $rules['images.' . $index] = 'image|mimes:jpeg,bmp,png|max:1000';
            }
        }
        $this->validate($request,$rules);
            $directory->user_id= Auth::user()->id;
            $directory->name= $request->name;
            $directory->category_id= $request->category;
            $directory->description= $request->description;
            $directory->fb_link= $request->fb_link;
            $directory->instagram_link= $request->insta_link;
        if(!empty($request->logo)){
            if($path=$request->file('logo')->store('logos')){
                $directory->logo= $path;
            }
        }
        if(!empty($request->banner)){
            if($path=$request->file('banner')->store('banners')){
                $directory->header= $path;
            }
        }
        if(!empty($request->images)){
            foreach ($request->images as $image) {
                $path = $image->store('gallery');
                DirectoryImage::create([
                    'directory_id' => $directory->id,
                    'link' => $path
                ]);
            }
        }
        if ($directory->save()){
            return redirect('directories')->with(['type'=>'success', 'message'=>'Directory Updated Successfully.']);
        }else{
            return redirect()->back()->with(['type'=>'error', 'message'=>'Directory Could not Updated.']);
        }
    }
    public function deleteDirectory($directory){
        $directory= Directory::findOrFail($directory);
        if ($directory->delete()){
            return redirect('directories')->with(['type'=>'success', 'message'=>'Directory Deleted Successfully.']);
        }else{
            return redirect()->back()->with(['type'=>'error', 'message'=>'Directory Could not Deleted.']);
        }
    }
}
