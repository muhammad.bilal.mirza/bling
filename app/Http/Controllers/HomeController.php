<?php

namespace App\Http\Controllers;

use App\Directory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $directories= Directory::where(['status'=>'active','favourite'=>'on'])->get();
        return view('home',compact('directories'));
    }
    public function directories($category=null)
    {
        if (!is_null($category)){
            $directories= Directory::where('category_id',$category)->get();
        }else{
            $directories= Directory::where('status','active')->get();
        }
        return view('directories',compact('directories'));
    }
    public function detail($directory)
    {
        $directory= Directory::findOrFail($directory);
        return view('detail',compact('directory'));
    }
}
