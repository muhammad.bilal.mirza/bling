<?php

namespace App\Http\Controllers;

use App\Category;
use App\Directory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function directories(){
        if (Auth::user()->role !== 'admin'){
            return redirect()->back();
        }
        $directories= Directory::with(['category','user'])->get();
        return view('admin.directory.list',compact('directories'));
    }
    public function editDirectory($directory){
        if (Auth::user()->role !== 'admin'){
            return redirect()->back();
        }
        $directory=Directory::findOrFail($directory);
        $categories= Category::all();
        return view('admin.directory.edit',compact('directory','categories'));
    }
    public function updateDirectory($directory,Request $request){
        if (Auth::user()->role !== 'admin'){
            return redirect()->back();
        }
        $directory=Directory::findOrFail($directory);
        $rules=[
            'name'=> 'required|string',
            'category'=> 'required|integer',
            'description'=> 'required|string',
            'status'=> 'required|string',
            'favourite'=> 'required|string',
            'fb_link'=> 'nullable|string',
            'insta_link'=> 'nullable|string',
            'logo'=> 'image|mimes:jpeg,jpg,png|max:1000',
            'banner'=> 'image|mimes:jpeg,jpg,png|max:1000',
        ];
        if(!empty($request->images)) {
            foreach ($request->images as $index => $item) {
                $rules['images.' . $index] = 'image|mimes:jpeg,bmp,png|max:1000';
            }
        }
        $this->validate($request,$rules);
        $directory->user_id= Auth::user()->id;
        $directory->name= $request->name;
        $directory->category_id= $request->category;
        $directory->description= $request->description;
        $directory->status= $request->status;
        $directory->favourite= $request->favourite;
        $directory->fb_link= $request->fb_link;
        $directory->instagram_link= $request->insta_link;
        if(!empty($request->logo)){
            if($path=$request->file('logo')->store('logos')){
                $directory->logo= $path;
            }
        }
        if(!empty($request->banner)){
            if($path=$request->file('banner')->store('banners')){
                $directory->header= $path;
            }
        }
        if(!empty($request->images)){
            foreach ($request->images as $image) {
                $path = $image->store('gallery');
                DirectoryImage::create([
                    'directory_id' => $directory->id,
                    'link' => $path
                ]);
            }
        }
        if ($directory->save()){
            return redirect('admin/directories')->with(['type'=>'success', 'message'=>'Directory Updated Successfully.']);
        }else{
            return redirect()->back()->with(['type'=>'error', 'message'=>'Directory Could not Updated.']);
        }
    }
    public function deleteDirectory($directory){
        if (Auth::user()->role !== 'admin'){
            return redirect()->back();
        }
        $directory= Directory::findOrFail($directory);
        if ($directory->delete()){
            return redirect('admin/directories')->with(['type'=>'success', 'message'=>'Directory Deleted Successfully.']);
        }else{
            return redirect()->back()->with(['type'=>'error', 'message'=>'Directory Could not Deleted.']);
        }
    }
}