<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DirectoryImage extends Model
{
    protected $guarded=[];
    public $timestamps=false;
    public function directory()
    {
        return $this->belongsTo(Directory::class);
    }
}
