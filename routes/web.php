<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'web'], function (){
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/directories/{category?}', 'HomeController@directories');
    Route::get('/directory/{directory}', 'HomeController@detail');
    Route::get('/contact', function () { return view('contact'); });
    Route::get('/about', function () { return view('about'); });
    Route::get('/blog', function () { return view('blog'); });
    Route::get('/list-with-us', function () { return view('list-with-us'); });
    Route::get('/categories', function () { $categories= \App\Category::all(); return view('categories',compact('categories')); });

    Auth::routes();

    /*UserController Routes*/
    Route::get('user-directories', 'UserController@directories');
    Route::get('add-directory', 'UserController@addDirectory');
    Route::post('directories', 'UserController@storeDirectory');
    Route::get('edit-directory/{directory}', 'UserController@editDirectory');
    Route::put('directories/{directory}', 'UserController@updateDirectory');
    Route::get('delete-directories/{directory}', 'UserController@deleteDirectory');

    /*AdminController Routes*/
    Route::get('admin/directories', 'AdminController@directories');
    Route::get('admin/edit-directory/{directory}', 'AdminController@editDirectory');
    Route::put('admin/directories/{directory}', 'AdminController@updateDirectory');
    Route::get('admin/delete-directories/{directory}', 'AdminController@deleteDirectory');
});
